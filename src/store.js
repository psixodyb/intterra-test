/* eslint-disable no-param-reassign */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */

import Vue from 'vue';
import Vuex from 'vuex';

import FieldService from '@/FieldService';
import Operation, { OperationType, Assessment } from '@/models/Operation';

const translate = require('@/locales/intterra-ru-RU.json');

const fieldService = new FieldService();

Vue.use(Vuex);


function addZero(number) {
  return number > 10 ? `${number}` : `${`${0}${number}`}`;
}


export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    sortBy: null,
    filterBy: null,
    reverse: false,
    operations: [],
  },
  mutations: {
    SET_OPERATIONS(state, payload) {
      state.operations = payload;
    },
    SET_SORT_BY(state, payload) {
      state.sortBy = payload;
    },
    SET_REVERSE(state, payload) {
      state.reverse = payload;
    },
    SET_FILTER_BY(state, payload) {
      state.filterBy = payload;
    },
  },
  actions: {
    async getOperations({ commit }) {
      const operations = await fieldService.getOperations();
      commit('SET_OPERATIONS', operations);
    },
  },
  getters: {
    getTypeName: state => type => translate[OperationType[type]],
    getAssessmentTranslate: state => assessment => translate[Assessment[assessment]] || 'Нет оценки',
    getSortedOperationsByDate: (state, getters) => {
      const result = [].concat(state.operations);
      return result.sort((a, b) => {
        const aDateString = `${a.date.year}${addZero(a.date.month)}${addZero(a.date.day)}`;
        const bDateString = `${b.date.year}${addZero(b.date.month)}${addZero(b.date.day)}`;
        if (aDateString < bDateString) {
          return state.reverse ? 1 : -1;
        }
        if (aDateString > bDateString) {
          return state.reverse ? -1 : 1;
        }
        return 0;
      });
    },
    getSortedOperationsByType: (state, getters) => {
      const result = [].concat(state.operations);
      return result.sort((a, b) => {
        const aTypeName = getters.getTypeName(a.type);
        const bTypeName = getters.getTypeName(b.type);
        if (aTypeName < bTypeName) {
          return state.reverse ? 1 : -1;
        }
        if (aTypeName > bTypeName) {
          return state.reverse ? -1 : 1;
        }
        return 0;
      });
    },
    getSortedOperationsByAssessment: (state, getters) => {
      const result = [].concat(state.operations);
      return result.sort((a, b) => {
        const firstAssesment = a.assessment === null ? -1 : a.assessment;
        const secondAssessment = b.assessment === null ? -1 : b.assessment;
        if (firstAssesment > secondAssessment) {
          return state.reverse ? -1 : 1;
        }
        if (firstAssesment < secondAssessment) {
          return state.reverse ? 1 : -1;
        }
        return 0;
      });
    },
    getFilteredOperations:
      state => sortedOperations => sortedOperations.filter((operation) => {
        const operationDateString = `${operation.date.year}${addZero(operation.date.month)}${addZero(operation.date.day)}`;
        const dateNow = new Date();
        const dateNowString = `${dateNow.getFullYear()}${addZero(dateNow.getMonth() + 1)}${addZero(dateNow.getDay())}`;
        if (state.filterBy === 'planned') return dateNowString < operationDateString;
        if (state.filterBy === 'complete') return dateNowString > operationDateString;
        return true;
      }),
    getSortedOperations: (state, getters) => {
      switch (state.sortBy) {
        case 'date':
          return getters.getFilteredOperations(getters.getSortedOperationsByDate);
        case 'type':
          return getters.getFilteredOperations(getters.getSortedOperationsByType);
        case 'assessment':
          return getters.getFilteredOperations(getters.getSortedOperationsByAssessment);
        default:
          return getters.getFilteredOperations(state.operations);
      }
    },
  },
});
